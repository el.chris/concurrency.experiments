#include <array>
#include <atomic>

// guarantee is
//      no data loss
//      data is in the right order
namespace LockFree
{
// strong single pop, single push SoFi
// weak multi pop, single push SoFi
template < typename Type, size_t Size >
class SoFi_weak3
{
  public:
    static constexpr size_t InternalSize = Size + 1;

    SoFi_weak3()
    {
        std::fill(storage.begin(), storage.end(), nullptr);
    }

    // weak, it is possible that push returns false if the fifo has space left
    // REQUIRED: in != nullptr
    bool push(const Type& in, Type& out)
    {
        auto l_writeIndex = writeIndex.load();
        storage[l_writeIndex % InternalSize].store(in);

        // TODO: bottleneck
        out = storage[(l_writeIndex + 1) % InternalSize].exchange(nullptr);
        // TODO: bottleneck end
        if (out != nullptr)
        {
            readIndex.fetch_add(1);
            return false;
        }
        writeIndex.store(l_writeIndex);
        return true;
    }

    // it gets weak if we have multi pop. in the single push, single pop case it
    // is strong,
    // if the fifo has elements and multiple pop methods are called concurrently
    // it is possible that some of the pop methods returning false even when
    // there are elements to pop
    bool pop(Type& out)
    {
        out = storage[readIndex.load() % InternalSize].exchange(nullptr);
        if (out != nullptr)
        {
            // if another pop thread performs an exchange when this thread
            // is interrupted here, it returns false
            // it can be also weak when push pop'ed an element
            //
            // it can
            // be made strong with an while true loop around this function
            // contents and an isEmpty check, so if sofi is not empty repeat
            // everything
            readIndex.fetch_add(1);
            return true;
        }

        return false;
    }

  private:
    static constexpr int  PageSize = 64;
    std::atomic< size_t > readIndex{0};
    char                  pad1[PageSize];
    std::atomic< size_t > writeIndex{0};
    char                  pad2[PageSize];

    std::array< std::atomic< Type >, InternalSize > storage;
};
} // namespace LockFree
