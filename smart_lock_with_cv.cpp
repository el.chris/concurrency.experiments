// for example
#include <iostream>
#include <vector>

// required for smart_lock
#include <condition_variable>
#include <functional>
#include <mutex>
#include <optional>

struct ForwardArgsToCTor_t {};
constexpr ForwardArgsToCTor_t ForwardArgsToCTor{};

template <typename T>
class smart_lock {
   private:
    class Proxy {
       public:
        Proxy(T& value, std::mutex& lock,
              const std::optional<std::condition_variable*> cv = std::nullopt);
        ~Proxy();

        Proxy(const Proxy&) = delete;
        Proxy(Proxy&&) = delete;
        Proxy& operator=(const Proxy&) = delete;
        Proxy& operator=(Proxy&&) = delete;

        T* operator->();
        const T* operator->() const;
        T& operator*();
        const T& operator*() const;

       private:
        T& value;
        std::mutex& lock;
        std::optional<std::condition_variable*> cv;
    };

    class ConstProxy {
       public:
        ConstProxy(
            const T& value, std::mutex& lock,
            const std::optional<std::condition_variable*> cv = std::nullopt);
        ~ConstProxy();

        ConstProxy(const ConstProxy&) = delete;
        ConstProxy(ConstProxy&&) = delete;
        ConstProxy& operator=(const ConstProxy&) = delete;
        ConstProxy& operator=(ConstProxy&&) = delete;

        const T* operator->() const;
        const T& operator*() const;

       private:
        const T& value;
        std::mutex& lock;
        std::optional<std::condition_variable*> cv;
    };

   public:
    smart_lock(const std::function<bool(const T&)>& predicate);

    smart_lock(const smart_lock& rhs);
    smart_lock(smart_lock&& rhs);
    smart_lock& operator=(const smart_lock& rhs);
    smart_lock& operator=(smart_lock&& rhs);

    ~smart_lock() = default;

    Proxy notify_after();
    void wait() const;

    Proxy operator->();
    ConstProxy operator->() const;

    Proxy get_scope_guard();
    ConstProxy get_scope_guard() const;

    T get_copy() const;

   private:
    T value;
    mutable std::mutex lock;
    std::function<bool(const T&)> predicate;
    mutable std::condition_variable cv;
};

template <typename T>
inline smart_lock<T>::smart_lock(const std::function<bool(const T&)>& predicate)
    : predicate(predicate) {}

template <typename T>
inline smart_lock<T>::smart_lock(const smart_lock& rhs)
    : value([&] {
          std::lock_guard<std::mutex> guard(rhs.lock);
          return rhs.value;
      }()) {}

template <typename T>
inline smart_lock<T>::smart_lock(smart_lock&& rhs)
    : value([&] {
          std::lock_guard<std::mutex> guard(rhs.lock);
          return std::move(rhs.value);
      }()) {}

template <typename T>
inline smart_lock<T>& smart_lock<T>::operator=(const smart_lock& rhs) {
    if (this != &rhs) {
        std::lock(this->lock, rhs.lock);
        std::lock_guard<std::mutex> guard(this->lock, std::adopt_lock);
        std::lock_guard<std::mutex> guardRhs(rhs.lock, std::adopt_lock);
        this->value = rhs.value;
    }

    return *this;
}

template <typename T>
inline smart_lock<T>& smart_lock<T>::operator=(smart_lock&& rhs) {
    if (this != &rhs) {
        std::lock(this->lock, rhs.lock);
        std::lock_guard<std::mutex> guard(this->lock, std::adopt_lock);
        std::lock_guard<std::mutex> guardRhs(rhs.lock, std::adopt_lock);
        this->value = std::move(rhs.value);
    }
    return *this;
}

template <typename T>
inline typename smart_lock<T>::Proxy smart_lock<T>::operator->() {
    return Proxy(this->value, this->lock);
}

template <typename T>
inline typename smart_lock<T>::ConstProxy smart_lock<T>::operator->() const {
    return ConstProxy(this->value, this->lock);
}

template <typename T>
inline typename smart_lock<T>::Proxy smart_lock<T>::notify_after() {
    return Proxy(this->value, this->lock, &this->cv);
}

template <typename T>
inline void smart_lock<T>::wait() const {
    std::unique_lock lock(this->lock);
    this->cv.wait(lock, [this] { return this->predicate(this->value); });
}

template <typename T>
inline typename smart_lock<T>::Proxy smart_lock<T>::get_scope_guard() {
    return Proxy(this->value, this->lock);
}

template <typename T>
inline typename smart_lock<T>::ConstProxy smart_lock<T>::get_scope_guard()
    const {
    return ConstProxy(this->value, this->lock);
}

template <typename T>
inline T smart_lock<T>::get_copy() const {
    std::lock_guard<std::mutex> guard(this->lock);
    return this->value;
}

// PROXY OBJECT

template <typename T>
inline smart_lock<T>::Proxy::Proxy(
    T& value, std::mutex& lock,
    const std::optional<std::condition_variable*> cv)
    : value(value), lock(lock), cv(cv) {
    this->lock.lock();
}

template <typename T>
inline smart_lock<T>::Proxy::~Proxy() {
    this->lock.unlock();
}

template <typename T>
inline T* smart_lock<T>::Proxy::operator->() {
    return &this->value;
}

template <typename T>
inline const T* smart_lock<T>::Proxy::operator->() const {
    return const_cast<smart_lock<T>::Proxy*>(this)->operator->();
}

template <typename T>
inline T& smart_lock<T>::Proxy::operator*() {
    return this->value;
}

template <typename T>
inline const T& smart_lock<T>::Proxy::operator*() const {
    return this->value;
}

template <typename T>
inline smart_lock<T>::ConstProxy::ConstProxy(
    const T& value, std::mutex& lock,
    const std::optional<std::condition_variable*> cv)
    : value(value), lock(lock), cv(cv) {
    this->lock.lock();
}

template <typename T>
inline smart_lock<T>::ConstProxy::~ConstProxy() {
    this->lock.unlock();
}

template <typename T>
inline const T* smart_lock<T>::ConstProxy::operator->() const {
    return &this->value;
}

template <typename T>
inline const T& smart_lock<T>::ConstProxy::operator*() const {
    return this->value;
}

template <typename T>
using ThreadSafeVector = smart_lock<std::vector<T>>;

int main() {
    ThreadSafeVector<int> vec([](auto& v) { return !v.empty(); });
    vec.wait();

    {
        auto guard = vec.get_scope_guard();
        for (auto& e : *guard) {
            std::cout << e << std::endl;
        }
    }
}
